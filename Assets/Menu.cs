﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour
{
    public Canvas MainCanvas;
    public Canvas OptionsCanvas;
    public Canvas Difficulty;
    public Canvas Story;
    public AudioSource Music;
    public Button SoundOn;
    public Button SoundOff;
    public Canvas HUD;
    public AudioSource MainMenuMusic;
    //public GameObject Health;
    public Vector3 spawnSpot = new Vector3(0, -20, 0);

    void Start()
    {
        Difficulty.enabled = false;
        Story.enabled = false;
        MainCanvas.enabled = true;
        MainMenuMusic.enabled = true;
        HUD.enabled = true;
        Music.enabled = false;
        OptionsCanvas.enabled = false;
        //Story.enabled = false;
        //Texture.enabled = false;
        //bool b = GameController.Instance.NeedsMenu();
        //bool m = GameController.Instance.NeedsHUD();
        //MainCanvas.enabled = b;
        //HUD.enabled = m;
    }

    public void ReturnOn()
    {
        OptionsCanvas.enabled = false;
        MainCanvas.enabled = true;
    }

    public void LoadOn()
    {
        //Application.LoadLevel("Platformer");
        Story.enabled = true;
        OptionsCanvas.enabled = false;
        MainCanvas.enabled = false;
        Difficulty.enabled = false;
        //HUD.enabled = true;
    }

    public void Begin()
    {
        Application.LoadLevel("Platformer");
    }

    public void DifficultyLoad()
    {
        Application.LoadLevel("Platformer");
        Difficulty.enabled = false;
        MainCanvas.enabled = false;
        HUD.enabled = true;
        Music.enabled = true;
        MainMenuMusic.enabled = false;
    }

    public void TurnSoundOff()
    {
        MainMenuMusic.enabled = false;
    }
    public void TurnSoundOn()
    {
        MainMenuMusic.enabled = true;
    }
    public void OptionsLoad()
    {
        MainCanvas.enabled = false;
        OptionsCanvas.enabled = true;
        Difficulty.enabled = false;
    }
    public void Return()
    {
        MainCanvas.enabled = true;
        OptionsCanvas.enabled = false;
        Difficulty.enabled = false;
    }
    public void DifficultyNormalLoad()
    {
        Application.LoadLevel("Platformer");
        Difficulty.enabled = false;
        MainCanvas.enabled = false;
        HUD.enabled = true;
        Music.enabled = true;
        MainMenuMusic.enabled = false;
        //GameObject cubeSpawn = (GameObject)Instantiate(Health, new Vector3(0, -2, 0), transform.rotation);
    }
    public void DifficultyEasyLoad()
    {
        Application.LoadLevel("Platformer");
        //Application.LoadLevel(Application.loadedLevel);
        Difficulty.enabled = false;
        MainCanvas.enabled = false;
        HUD.enabled = true;
        Music.enabled = true;
        MainMenuMusic.enabled = false;
        //GameObject cubeSpawn = (GameObject)Instantiate(Health, new Vector3(0, -2, 0), transform.rotation);
        //GameObject cubeSpawn1 = (GameObject)Instantiate(Health, new Vector3(0, -2, 0), transform.rotation);

    }
    void GameOver()
    {
        MainMenuMusic.enabled = true;
        HUD.enabled = false;
        Music.enabled = false;
        OptionsCanvas.enabled = false;
        //Texture.enabled = false;
        //bool b = GameController.Instance.NeedsMenu();
        //bool m = GameController.Instance.NeedsHUD();
        //MainCanvas.enabled = b;
        //HUD.enabled = m;
    }
}
