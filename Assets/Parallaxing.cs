﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {

    public Transform[] backgrounds; //Array Of All Backgrounds.(Parallax)
    private float[] ParallaxScales; //Camera porportion parallax base.
    public float smoothing = 1f;    //Make sure to set over 0.

    private Transform cam;          //Reference to main camera transform.
    private Vector3 previousCamPos; //Cameras pos in previous frame.

    void Awake()
    {
        //set up camera as a reference.
        cam = Camera.main.transform;
    }
	// Use this for initialization
	void Start () {
        previousCamPos = cam.position;
        ParallaxScales = new float[backgrounds.Length];

        for (int i = 0; i < backgrounds.Length; i++)
        {
            ParallaxScales[i] = backgrounds[i].position.z * -1;
        }
	
	}

    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i < backgrounds.Length; i++)
        {
            float parallax = (previousCamPos.x - cam.position.x) * ParallaxScales[i];
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;
            Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z );
            backgrounds[i].position = Vector3.Lerp (backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);

        }
        previousCamPos = cam.position;
    }
}
